<?php
/**
 * Created by PhpStorm.
 * User: kridah
 * Date: 2019-02-20
 * Time: 10:25
 */

class User
{
    private static $KEY_SESSION_USERID = 'userid';
    private static $KEY_SESSION_ACCOUNT_TYPE = 'account_type';

    /**
     * @param $db
     * @param $fullname
     * @param $email
     * @param $password
     * @param $account_type_request
     * @return int|string
     */
    static function create($db, $fullname, $email, $password, $account_type_request) {

        $query = "INSERT INTO user (fullname, email, password, account_type_request)
                            VALUES(?, ?, ?, ? ,?, ?)";

        // id is auto_increment and account_type has default student
        $user_id = uniqid();
        $param = array($user_id, $fullname, $email, User::getHashedPassword($password), $account_type_request);
        $stmt = $db->prepare($query);
        $stmt->execute($param);

        /** @error If error inserting user in db */
        if ($stmt->rowCount() != 1) {
            return 0;
        }
        return $user_id;
    }

    /**
     * @param $db
     * @param $email
     * @param $password
     * @return int
     */
    static function login($db, $email, $password) {
        $query = "SELECT * FROM user WHERE email = (?)";
        $param = array($email);
        $stmt = $db->prepare($query);
        $stmt->execute($param);

        /* If function returns <> 1 row (of users), assume correct username */
        if ($stmt->rowCount() != 1) {
            return 0;
        }

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        /** if password is matches */
        if (!password_verify($password, $user['password'])) {
            return 0;
        }

    }

    /**
     * @return int
     */
    static function get_logged_in_user() {
        User::require_session();

        if (!isset($_SESSION[User::$KEY_SESSION_USERID])) {
            return 0;
        }
        return $_SESSION[User::$KEY_SESSION_USERID];
    }


    static function require_session() {
        if (sessions_status() == PHP_SESSIONS_NONE) {
            @sessions_start();
        }
    }


    /**
     * @param $password
     * @return bool|string
     */
    static function hash_password($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}