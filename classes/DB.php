<?php
/**
 * Created by PhpStorm.
 * User: kridah
 * Date: 2019-02-20
 * Time: 10:26
 */

class DB
{

    private static $db = null;
    private $dsn = 'mysql:dbname=ntnutube;host=127.0.0.1';
    private $user = 'root';
    private $password = 'toor';
    private $dbh = null;

    private function __construct() {
        try {
        $this->dbh = new PDO($this->dsn, $this->user, $this->password);

        } catch (PDOException $e) {
            // IKKE BRUK I PRODUKSJON
            echo 'Tilkobling feilet' . $e->getMessage();
        }
    }

    public static function getDBConnection() {
        if (DB::$db == null) {
            DB::$db = new self();
        }
        return DB::$db->dbh;
    }
}