<?php
/**
 * Created by PhpStorm.
 * User: kridah
 * Date: 2019-02-26
 * Time: 14:29
 */
$ROOT = $_SERVER['DOCUMENT_ROOT'];

$require_once ="$ROOT/classes/DB.php";

$user_id = User::login($db, $email, $password);
$save_credentials = isset($_POST['save_credentials']);

if($save_credentials) {
    $TTRM = time() + (86400 * 30);      // TimeToRemember = localtime + 30 days
    set_cookie('email', $email, $TTRM);
}