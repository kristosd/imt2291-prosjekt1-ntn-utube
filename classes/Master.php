<?php
/**
 * Created by PhpStorm.
 * User: kridah
 * Date: 2019-02-26
 * Time: 20:17
 *
 * Mother class that handles class requirements etc...
 */
$ROOT = $_SERVER['DOCUMENT_ROOT'];
require_once "$ROOT/vendor/autoload.php";
require_once "$ROOT/classes/DB.php";
require_once "$ROOT/classes/User.php";


class Master {


    public static function require_twig(){
        $ROOT = $_SERVER['DOCUMENT_ROOT'];
        $loader = new Twig_Loader_Filesystem("$ROOT/twig");
        return new Twig_Environment($loader, array(

        ));
    }

    public static function require_database(){
        $db = DB::getDBConnection();

        return $db;
    }

}
