<?php
/**
 * Created by PhpStorm.
 * User: kridah
 * Date: 2019-02-26
 * Time: 20:06
 */

$ROOT = $_SERVER['DOCUMENT_ROOT'];
require_once "$ROOT/classes/Master.php";

$db = Master::require_database();
$twig = Master::require_twig();
$user_id = User::get_logged_in_user();

echo $twig->render('index.html.twig');

